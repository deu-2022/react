FROM node:16-alpine3.11 as node
RUN mkdir -p /app && chown node:node /app
USER node
WORKDIR /app
COPY package.json ./
COPY package-lock.json ./
COPY --chown=node . .
RUN npm ci && npm run build

FROM node:16-alpine3.11 as utils-node
RUN mkdir /.npm && chmod 777 /.npm
RUN mkdir -p /app && chown node:node /app
USER node
WORKDIR /app

FROM nginx:1.21-alpine as nginx
COPY docker/nginx/nginx.conf /etc/nginx/conf.d/default.conf
COPY --from=node /app/build /usr/share/nginx/html
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]