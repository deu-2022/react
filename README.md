# Simulación inundaciones La Plata

**UNLP** - **DEU y DCU** - **2022** - **Grupo 6**

Esta aplicación es una herramienta desarrollada en el contexto de la materia
*Diseño de Experiencia de Usuario y Diseño Centrado en el Usuario* de Facultad
de Informática perteneciente a la Universidad Nacional de La Plata.

El objetivo del trabajo es materializar los conceptos aprendidos en un producto
de software, siendo la temática general de todos los grupos las inundaciones en
nuestra ciudad.

Para cumplir este objetivo nuestro grupo decidió desarrollar una aplicación web,
que tiene como función principal simular una posible inundación en la ciudad.

Se utilizó [React](https://reactjs.org/) como base para el desarrollo. Los mapas
se obtienen de [Mapbox](https://www.mapbox.com/) y se integran en React con la
librería [react-map-gl](https://visgl.github.io/react-map-gl/).

La interfaz de usuario se logra con la ayuda de
[Material-UI](https://material-ui.com/). Por último dentro de la aplicación se
genera un recorrido de la misma para guiar al usuario. Esto se logra con la
librería [react-joyride](https://github.com/gilbarbara/react-joyride)

## Demo de la aplicación en producción

* [app-inundaciones-deu.vercel.app](https://app-inundaciones-deu.vercel.app/)
  
  [![demo-vercel](https://app-inundaciones-deu.vercel.app/favicon.ico)](https://app-inundaciones-deu.vercel.app/)

[![pipeline status](https://gitlab.com/deu-2022/react/badges/main/pipeline.svg)](https://gitlab.com/deu-2022/react/-/commits/main)
[![coverage report](https://gitlab.com/deu-2022/react/badges/main/coverage.svg)](https://gitlab.com/deu-2022/react/-/commits/main)
[![Latest Release](https://gitlab.com/deu-2022/react/-/badges/release.svg)](https://gitlab.com/deu-2022/react/-/releases)

## Requerimientos para el desarrollo

* Docker (<=20.10.12)
* Docker-Compose (<=2.2.3)
* Direnv (<=2.28.0)

### Direnv

Utilizando la herramienta [direnv](https://direnv.net/), podemos configurar
algunas variables de entorno que se utilizan en nuestro proyecto. Para ello
proveemos el archivo [`.envrc`](.envrc) que contiene dicha configuración.

Una vez instalado direnv se debe ejecutar `direnv allow` en la raíz del proyecto
para cargar la configuración. Cualquier otra variable que se desee configurar en
el entorno deberá ser agregada al archivo `.envrc`.

Este archivo se puede versionar siempre y cuando no contenga datos sensibles. En
ese caso se recomienda agregar el `.envrc` a .gitignore y subir otro archivo
`.envrc-sample` como una versión de referencia, que luego cada desarrollador
completará con los datos reales que le sean compartidos por un medio seguro.

Para comprobar que esta instalado correctamente direnv, deberemos ingresar a la
carpeta del proyecto y ejecutar el comando `which npm` y debería mostrarnos
uno en la carpeta del proyecto.

### Scripts

En la carpeta [`docker/bin`](docker/bin) se encuentran los scripts desarrollados
para simplificar el uso de docker-compose. Gracias a lo realizado en el paso
anterior con direnv, los mismos se encuentran en el `PATH`, por lo que pueden
ser ejecutados directamente.

Estos son:

* `docker-prod`: Es un wrapper para docker-compose que permite ejecutar el
  proyecto en modo producción.

* `docker-dev`: Permite ejecutar el proyecto en modo desarrollo.

* `npm`: Permiten ejecutar las instrucciones de para instalar paquetes en el
  entorno de desarrollo.

> **Nota**: Para poder ejecutar correctamente estos wrappers, se debe utilizar
> docker con un usuario no privilegiado. Es decir, no funcionarán
> correctamente si uno debe ejecutar los comandos de docker con `sudo`.
> Para resolver esta situación seguir la
> [documentación de docker](https://docs.docker.com/engine/install/linux-postinstall/).

## Producción

Para probar el proyecto en producción, alcanza con ejecutar el comando
`docker-prod up`.

## Desarrollo

Para desarrollo, se debe ejecutar el script `docker-dev up`. Sin embargo, para
que este funcione la primera vez, se deben instalar manualmente los paquetes
necesarios. Esto se hace desde la raíz del repositorio, ya que gracias a los
scripts descriptos anteriormente es como correrlos dentro del contenedor.

> **Nota**: Este comando deberá ejecutarse desde otra terminal, a menos que se
> inicien los contenedores con `docker-dev up -d` en cuyo caso podrá realizarse
> desde la misma consola.

```bash
# Instalar paquetes de desarrollo
npm ci
```

> **Nota**: Es de esperarse que el servicio `node-dev` falle al iniciarse por
> primera vez. Esto se debe a que el bind mount de la carpeta de desarrollo
> contiene una carpeta `node_modules` vacía. Luego de correr `npm ci`, se
> se descargan las dependencias y gracias a la política de restart `on-failure`,
> el servicio se reiniciará automáticamente.

### Comandos de utilidad

A continuación se listaran algunos comandos de utilidad para utilizar en el
proyecto:

```bash
# Para ver el estado de los contenedores
docker-prod/dev ps

# Para regenerar las imágenes (en caso de incorporar cambios)
docker-prod/dev build

# Para mirar los logs en un contenedor
docker-prod/dev logs <nombre-contenedor>
# Ejemplo: docker-dev logs node-dev

# Para detener los contenedores
docker-prod/dev down

# Detener los contenedores eliminando los volúmenes
docker-prod/dev down -v

# Iniciar los contenedores reconstruyendo las imágenes y detacheando la termminal
docker-prod/dev up --build -d
```

## Despliegue

Cada vez que se actualice la rama **main** en este repositorio se correrá un
[pipeline](-/pipelines) que, usando [Vercel](https://vercel.com/), construye
la imagen y la despliega en
[app-inundaciones-deu.vercel.app](https://app-inundaciones-deu.vercel.app/)

[![demo-vercel](https://app-inundaciones-deu.vercel.app/favicon.ico)](https://app-inundaciones-deu.vercel.app/)
