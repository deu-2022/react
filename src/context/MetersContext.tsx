import { createContext, useState } from "react";

const MetersContext = createContext({
    setSliderMeters: (meters: number) => {},
    meters: 0
})

function MetersProviderContext({children}: {children: React.ReactNode}) {
  const [meters, setMeters] = useState<number>(0);
  const setSliderMeters = (meters: number) => setMeters(meters);

  return (
    <MetersContext.Provider value={{ setSliderMeters, meters }}>
      {children}
    </MetersContext.Provider>
  );
}

export {MetersContext, MetersProviderContext}