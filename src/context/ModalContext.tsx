import { createContext, PropsWithChildren, useState } from "react";

export const ModalContext = createContext({
  toggle: () => {},
  open: false,
});

function ModalProvider({ children }: PropsWithChildren) {
  const [open, setOpen] = useState(false);
  const toggle = () => {
    console.log("toggle");
    setOpen(!open);
  };
  return (
    <ModalContext.Provider value={{ toggle, open }}>
      {children}
    </ModalContext.Provider>
  );
}

export { ModalProvider };
