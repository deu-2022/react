import { Box, BoxProps, Button, Divider } from '@mui/material';
import Joyride, { CallBackProps, STATUS, Step, Locale } from 'react-joyride';
import { useMount, useSetState } from 'react-use';
import { logGroup } from 'modules/helpers';
import { useState, useEffect, Dispatch, SetStateAction } from 'react';
import { useTheme } from '@mui/material';
import useModalContext from 'hooks/UseModalContext';

interface Props {
  breakpoint: string;
}

interface State {
  run: boolean;
  steps: Step[];
}

const LOCALE: Locale = {
  back: 'Atrás',
  close: 'Cerrar',
  last: 'Último',
  next: 'Siguiente',
  open: 'Abrir',
  skip: <strong>SALTAR</strong>,
}

export default function BasicDemo(props: Props) {
  const { breakpoint } = props;

  useEffect(() => {

    if (!localStorage["alreadyVisited"]) {
      localStorage["alreadyVisited"] = true;
      setState( {run: true} )
    }
  }, [localStorage["alreadyVisited"]]);

  const [{ run, steps }, setState] = useSetState<State>({
    run: false,
    steps: [
      {
        content: (
          <div>
            <h2> Bienvenido a la Simulación de Inundaciones</h2>
            <p>
              Esta aplicación es una herramienta para simular una posible
              inundación en la ciudad de <strong>La Plata</strong>
            </p>
          </div>
        ),
        placement: 'center',
        target: 'body',
        locale: LOCALE,
      },
      {
        content: (
          <div>
            <h2>Primero, un recorrido por la aplicación</h2>
            <p>
              Te guiaremos por las distintas herramientas que tenemos para que
              puedas usarlas de la mejor manera.
            </p>
            <p>
              En cualquier momento puedes <em> clickear la X </em>
              para pausar la explicación y continuar luego.
            </p>
            <p>
              También puedes omitirlo completamente presionando <em>Saltar</em>.
            </p>
          </div>
        ),
        placement: 'center',
        target: 'body',
        locale: LOCALE,
      },
      {
        content: (
          <div>
            <h2>Simulador de altura de agua</h2>
            <p>Cambiando este valor se indica la cantidad de metros
              de agua que hay luego de las lluvias.
              En el mapa se reflejará una representación del nivel del agua
              que podría llegar a acumularse en las calles.
            </p>
          </div>
        ),
        placement: 'left',
        target: '.Slider_Demo',
        locale: LOCALE,
      },
      {
        content: (
          <div>
            <p> También se pueden utilizar estos controles para cambiar el valor.</p>
          </div>
        ),
        placement: 'left',
        target: '.Slider_Controls_Demo',
        locale: LOCALE,
      },
      {
        content: (
          <div>
            <h2>Buscar una dirección</h2>
            <p>Para acceder a una dirección mediante la calle y el número.
            </p>
          </div>
        ),
        placement: 'right',
        target: '.Search_Demo',
        locale: LOCALE,
      },
      {
        content: (
          <div>
            <h2>Condiciones archivadas</h2>
            <p>Podrás visualizar cómo fué la situación en varios
              sucesos históricos.
            </p>
          </div>
        ),
        placement: 'right',
        target: '.Archive_Demo',
        locale: LOCALE,
      },
      {
        content: (
          <div>
            <h2>Configuraciones personalizadas</h2>
            <p>Aquí podrás cambiar algunos ajustes para personalizar tu
              experiencia.
            </p>
          </div>
        ),
        placement: 'right',
        target: '.Config_Demo',
        locale: LOCALE,
      },
      {
        content: (
          <div>
            <h2>Acerca de nosotros</h2>
            <p>Aquí obtendrás más información sobre el proyecto. También podrás
              reiniciar este recorrido.
            </p>
          </div>
        ),
        placement: 'right',
        target: '.Help_Demo',
        locale: LOCALE,
      },
      {
        content: (
          <div>
            <h2>Acceder a tu ubicación</h2>
            <p>Este botón sirve para centrar el mapa en la ubicación de éste
              dispositivo.
            </p>
          </div>
        ),
        placement: 'right',
        target: '.mapboxgl-ctrl-geolocate',
        locale: LOCALE,
      }
    ]
  });
  const {toggle} = useModalContext();
  const handleClickStart = (event: React.MouseEvent<HTMLElement>) => {
    event.preventDefault();

    setState({
      run: true,
    });
    toggle();
  };



  const handleJoyrideCallback = (data: CallBackProps) => {
    const { status, type } = data;
    const finishedStatuses: string[] = [STATUS.FINISHED, STATUS.SKIPPED];

    if (finishedStatuses.includes(status)) {
      setState({ run: false });
    }
    
    logGroup(type, data);
    
  };

  const theme = useTheme();

  return (
    <Box>
      <Joyride
        callback={handleJoyrideCallback}
        continuous
        //hideBackButton
        //hideCloseButton
        run={run}
        scrollToFirstStep
        showProgress
        showSkipButton
        steps={steps}
        styles={{
          options: {
            // arrowColor: theme.palette.primary.main,
            // backgroundColor: '#fff',
            beaconSize: 50,
            overlayColor: 'rgba(0, 0, 0, 0.5)',
            primaryColor: theme.palette.primary.main,
            spotlightShadow: '0 0 15px rgba(0, 0, 0, 0.5)',
            //textColor: '#333',
            zIndex: 10000,
          },
        }}
      />
      <Button onClick={handleClickStart} sx={{ mr: 1 }} color={"primary"} variant="contained">
        Volver a hacer el tour
      </Button>
    </Box>
  );
}