import { Box, Button, Paper, SwipeableDrawer, Typography } from "@mui/material";
import useModalContext from "hooks/UseModalContext";
import { PropsWithChildren, useState } from "react";
import { isMobile } from "react-device-detect";

interface Props {
  text: string;
  classNameButton?: string;
  icon: any;
  variant?: "text" | "outlined" | "contained" | undefined;
}

export default function MenuModalButtonContext(
  props: PropsWithChildren<Props>
) {
  let { text, icon, variant, children, classNameButton } = props;
  const { toggle, open } = useModalContext();
  const [showButtonText, showText] = useState(false);

  function onClickHandler() {
    toggle();
  }

  return (
    <Box sx={{ ...(!isMobile && { display: "flex", flexDirection: "row" }) }}>
      <Button
        onMouseEnter={() => showText(true)}
        onMouseLeave={() => showText(false)}
        className={classNameButton}
        aria-label={text}
        onClick={onClickHandler}
        sx={{ ...(!isMobile ? { m: 2 } : { m: 0.7 }), p: 2, maxHeight: 60 }}
        variant={variant}
        color="secondary"
      >
        {icon}
        {showButtonText && !isMobile && (
          <Typography
            sx={{ ml: 1, textTransform: "capitalize" }}
            variant="button"
          >
            {text}
          </Typography>
        )}
      </Button>
      {open &&
        (!isMobile ? (
          <Paper sx={{ backgroundColor: "secondary" }}>{children}</Paper>
        ) : (
          <SwipeableDrawer
            sx={{
              "& .MuiDrawer-paper": {
                borderEndEndRadius: 0,
                borderEndStartRadius: 0,
              },
            }}
            anchor="bottom"
            open={open}
            onClose={onClickHandler}
            onOpen={onClickHandler}
            disableSwipeToOpen={false}
            ModalProps={{
              keepMounted: true,
            }}
          >
            {children}
          </SwipeableDrawer>
        ))}
    </Box>
  );
}
