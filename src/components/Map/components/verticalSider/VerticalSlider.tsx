import * as React from "react";
import { useEffect, useState } from "react";
import Box from "@mui/material/Box";
import Slider from "@mui/material/Slider";
import Button from "@mui/material/Button";
import { Paper, Grid } from "@mui/material";
import { isMobile } from 'react-device-detect';
import AddIcon from '@mui/icons-material/Add';
import RemoveIcon from '@mui/icons-material/Remove';
import useMetersContext from "hooks/UseMetersContext";
import { MAX_WATER_HEIGHT } from "config/Constant";

export const MAX_M = MAX_WATER_HEIGHT;
export const MIN_M = 0;
export const SCALE_STEP = (MAX_M - MIN_M)/20;

// @ts-ignore (typescript)
function VerticalSlider(props) {
  const {setSliderMeters, meters} = useMetersContext()
  const { onChangeHeight: onChangeSlider } = props;
  
  useEffect(() => {onChangeSlider(meters)}, [meters]) 

  // @ts-ignore (typescript)
  const onSliderMoved = (evt) => {
    setSliderMeters(evt.target.value);
  };

  function changeValue(increase: boolean) {
    let newValue: number = meters+ (increase ? SCALE_STEP : -SCALE_STEP);
    if (newValue >= MIN_M && newValue <= MAX_M) {
      setSliderMeters(newValue);
    }
  }

  //Esta parte no se borra
  let marks: { value: number; label: string }[] = [];
  for (let i = MIN_M; i <= MAX_M; i += SCALE_STEP) {
    marks.push({ value: i, label: `${i} m` });
  }

  function preventHorizontalKeyboardNavigation(event: React.KeyboardEvent) {
    if (event.key === "ArrowLeft" || event.key === "ArrowRight") {
      event.preventDefault();
    }
  }
  
  function valuetext(value: number) {
    return `${value} m`;
  }

  return (
    <Box sx={{ position: "absolute", top: 0, right: "30px" }}>
      <Paper
        className={"Slider_Demo"}
        sx={{
          margin: "20px auto 20px auto",
          height: (isMobile) ? "55vh" : "80vh",
          minWidth: (isMobile) ? "30px" : "120px",
          maxWidth: "10vw",
          zIndex: 1,
          width: "10vw",
          display: "flex",
          alignItems: "center",
          justifyContent: "center",
          borderRadius: "25px",
          backgroundColor: "secondary",
        }}
      >
        <Slider
          sx={{
            height: "90%",
            '& input[type="range"]': {
              WebkitAppearance: "slider-vertical",
            },
          }}
          orientation="vertical"
          value={meters}
          valueLabelFormat={valuetext}
          max={MAX_M}
          min={MIN_M}
          step={1}
          aria-label="Meters"
          marks={(!isMobile) ? marks : []}
          valueLabelDisplay={!isMobile ? "auto" : "on"}
          onKeyDown={preventHorizontalKeyboardNavigation}
          onChange={onSliderMoved}
        />
      </Paper>
      <Grid className={"Slider_Controls_Demo"} container spacing={isMobile ? 2 : 0} direction={isMobile ? "column" : "row"}>
        <Grid item xs={6}>
          <Button sx={{ minWidth: (isMobile ? 50 : 65) }} onClick={() => { changeValue(true); }} variant="contained">
            <AddIcon/>
          </Button>
        </Grid>
        <Grid item xs={6}>
          <Button sx={{ minWidth: (isMobile ? 50 : 65) }} onClick={() => { changeValue(false); }} variant="contained">
            <RemoveIcon/>
          </Button>
        </Grid>
      </Grid>
    </Box >
  );
}

export default React.memo(VerticalSlider);
