import MapGL, { Source, Layer, GeolocateControl, MapRef } from 'react-map-gl';
// @ts-ignore (typescript)
import VerticalSlider from './components/verticalSider/VerticalSlider';
import 'mapbox-gl/dist/mapbox-gl.css'
import React, { useState, useRef, useCallback } from "react";
// @ts-ignore (typescript)
import MapboxGeocoder from '@mapbox/mapbox-gl-geocoder';
import { MAPBOX_TOKEN, MAP_STYLE_DARK, MAP_STYLE_LIGHT, MAX_WATER_HEIGHT } from 'config/Constant';
import type { SkyLayer, FillExtrusionLayer } from 'react-map-gl';
import { useTheme } from '@mui/material';
import { isMobile } from "react-device-detect";

export default function Mapita() {
  const [selectedM, selectM] = useState(0);

  const skyLayer: SkyLayer = {
    id: 'sky',
    type: 'sky',
    paint: {
      'sky-type': 'atmosphere',
      'sky-atmosphere-sun': [0.0, 0.0],
      'sky-atmosphere-sun-intensity': 15
    }
  };

  const buildingLayer: FillExtrusionLayer = {
    id: 'building',
    type: 'fill-extrusion',
    source: 'composite',
    'source-layer': 'building',
    paint: {
      'fill-extrusion-color': '#000',
      'fill-extrusion-height': [
        'interpolate',
        ['linear'],
        ['zoom'],
        15,
        0,
        15.05,
        ['get', 'height']
      ],
      'fill-extrusion-base': [
        'interpolate',
        ['linear'],
        ['zoom'],
        15,
        0,
        15.05,
        ['get', 'min_height']
      ],
      'fill-extrusion-opacity': 0.6
    }
  };

  const MAP_CENTER = [-57.95, -34.93]
  const MAP_ZOOM = 13

  // Crear cuatro puntos para un rectangulo desde el centro
  // RECTANGLE = [ancho, alto]
  // const RECTANGLE = [0.1, 0.05]
  // const NORTH_BOUND = MAP_CENTER[1] + RECTANGLE[1]
  // const SOUTH_BOUND = MAP_CENTER[1] - RECTANGLE[1]
  // const EAST_BOUND = MAP_CENTER[0] + RECTANGLE[0]
  // const WEST_BOUND = MAP_CENTER[0] - RECTANGLE[0]

  // Si no, de esta forma se puede abarcar todo el globo:
  const NORTH_BOUND = 90.0
  const SOUTH_BOUND = -90.0
  const EAST_BOUND = 180.0
  const WEST_BOUND = -180.0


  const mapRef = useRef();
  // @ts-ignore (typescript)
  //const mapGL = mapRef.getMap();
  const bounds = mapRef.current;
  const waterSquare = {
    type: 'FeatureCollection',
    features: [
      {
        type: 'Feature',
        geometry: {
          type: 'Polygon',
          coordinates: [
            [
              [WEST_BOUND, NORTH_BOUND], [EAST_BOUND, NORTH_BOUND],
              [EAST_BOUND, SOUTH_BOUND], [WEST_BOUND, SOUTH_BOUND],
              [WEST_BOUND, NORTH_BOUND]
            ]
          ]
        }
      }
    ]
  };

  const WATER_COLOR = '#75cff0';

  function calculateWaterHeight(value: number) {
    // Acá se calcularía la altura del agua,
    // Por ahora se retorna el valor ajustado al máximo
    let v = value - 5;
    return v < 0 ? 0 : v; 
  }


  function calculateWaterOpac(value: number) {
    const max_opacity = 0.8;
    // Ajusto el valor a una opacidad para que vaya subiendo cuadráticamente
    // respecto del slider, hasta llegar a la mitad, donde toma la máxima opacidad
    let threshold = MAX_WATER_HEIGHT / 4;
    return (value < threshold ? Math.pow(value, 2) / Math.pow(threshold, 2) : 1) * max_opacity;
  }

  const waterLayer = {
    id: 'waterLayer',
    type: 'fill-extrusion',
    paint: {
      'fill-extrusion-base': 0,
      'fill-extrusion-height': calculateWaterHeight(selectedM),
      'fill-extrusion-color': WATER_COLOR,
      'fill-extrusion-opacity': calculateWaterOpac(selectedM),
      'fill-extrusion-vertical-gradient': true
    }
  };

  const geocoderContainerRef = useRef();
  //const mapRef = useRef();
  const theme = useTheme();

  return (
    <>
      <MapGL
        initialViewState={{
          latitude: MAP_CENTER[1],
          longitude: MAP_CENTER[0],
          zoom: MAP_ZOOM,
          pitch: 30,
          bearing: 0
        }}
        mapStyle={theme.palette.mode === 'light' ? MAP_STYLE_LIGHT : MAP_STYLE_DARK}
        mapboxAccessToken={MAPBOX_TOKEN}
        maxPitch={85}
        terrain={{ source: 'mapbox-dem', exaggeration: 2 }}
        attributionControl={true}
        // projection="globe"

        fog={{
          "range": [-0.5, 10],
          "color": "#5d6066",
          "horizon-blend": 0.1
        }}
      >
        <GeolocateControl
          positionOptions={{ enableHighAccuracy: true }}
          trackUserLocation={true}
          position={(isMobile) ? 'top-left' : 'bottom-left'}
          style={{
            ...(!isMobile ? {
              margin: '20px',
              padding: '10px'
            } : {
              margin: '10px',
              padding: '5px'
            })
          }}
        />
        {/* <MapboxGeocoder/> No anda :( */}

        <Source
          id="mapbox-dem"
          type="raster-dem"
          url="mapbox://mapbox.mapbox-terrain-dem-v1"
          tileSize={512}
          maxzoom={14}
        />
        {// @ts-ignore (typescript)
          <Source id="waterSource" type="geojson" data={waterSquare}>
            {// @ts-ignore (typescript)
              <Layer {...waterLayer} />
            }
          </Source>
        }
        <Layer {...skyLayer} />
        <Layer {...buildingLayer} />
      </MapGL>
      <VerticalSlider
        onChangeHeight={selectM}
      />
    </>
  );
}
