import {
  Button,
  FormHelperText,
  Grid,
  Input,
  InputLabel,
  Switch,
  Typography,
  useTheme,
} from "@mui/material";
import { Box } from "@mui/system";
import MenuModalButton from "components/MenuModalButton/MenuModalButton";
import SettingsIcon from "@mui/icons-material/Settings";
import QuestionMarkIcon from "@mui/icons-material/QuestionMark";
import SearchIcon from "@mui/icons-material/Search";
import MyLocationIcon from "@mui/icons-material/MyLocation";
import DriveFileMoveIcon from "@mui/icons-material/DriveFileMove";
import ArrowForwardIcon from "@mui/icons-material/ArrowForward";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import AddIcon from "@mui/icons-material/Add";
import RemoveIcon from "@mui/icons-material/Remove";
import { useContext } from "react";
import { ThemeContext } from "context/ThemeContext";
import { Link } from "react-router-dom";
import BasicDemo from "../Demo/Demo";
import { useState } from "react";
import { getScreenSize } from "modules/helpers";
import { isMobile } from "react-device-detect";
import { ModalProvider } from "context/ModalContext";
import MenuModalButtonContext from "components/MenuModalButton/MenuModalButtonContext";
import useModalContext from "hooks/UseModalContext";
import CondicionesArchivadas from "./components/CondicionesArchivadas";

export default function Menu() {
  const [breakpoint, setBreakpoint] = useState(getScreenSize());
  const { toggleColorMode, incrementFontSize, decrementFontSize } =
    useContext(ThemeContext);
  const theme = useTheme();
  const { toggle } = useModalContext();
  return (
    <Grid
      direction={!isMobile ? "column" : "row"}
      container
      justifyContent="center"
      alignItems="flex-start"
    >
      <Grid item>
        <MenuModalButton
          classNameButton={`Search_Demo`}
          text="Buscar ubicación"
          icon={<SearchIcon />}
          variant="contained"
        >
          <Box sx={{ m: 5 }}>
            <InputLabel htmlFor="my-input" color="primary" disabled>
              Dirección
            </InputLabel>
            <Input id="my-input" aria-describedby="my-helper-text" disabled />
            <FormHelperText id="my-helper-text" disabled>
              Indique la calle, el numero/altura que desea buscar.
            </FormHelperText>
            <Button
              aria-label="Buscar"
              onClick={() => {}}
              sx={{ p: 2, mt: 2 }}
              endIcon={<ArrowForwardIcon />}
              color="primary"
              variant="contained"
              fullWidth
              disabled
            >
              <Typography variant="button">Buscar</Typography>
            </Button>
          </Box>
        </MenuModalButton>
      </Grid>
      <Grid item>
        <MenuModalButton
          text="Condiciones Archivadas"
          icon={<DriveFileMoveIcon />}
          classNameButton={`Archive_Demo`}
          variant="contained"
        >
          <CondicionesArchivadas />
        </MenuModalButton>
      </Grid>
      <Grid item>
        <MenuModalButton
          text="Configuración"
          icon={<SettingsIcon />}
          classNameButton={`Config_Demo`}
          variant="contained"
        >
          <Box sx={{ m: 5 }}>
            <Box>
              <Typography variant="h3" sx={{ fontWeight: "bold", pb: 5 }}>
                Configuración
              </Typography>
            </Box>
            <Box sx={{ display: "flex", flexDirection: "row" }}>
              <Typography variant="h5" sx={{ fontWeight: "medium" }}>
                Modo oscuro
              </Typography>
              <Switch
                value="checkedA"
                checked={theme.palette.mode === "dark"}
                inputProps={{ "aria-label": "Modo oscuro" }}
                onChange={toggleColorMode}
              />
            </Box>
            <Box
              sx={{
                display: "flex",
                flexDirection: "row",
                alignContent: "center",
              }}
            >
              <Typography variant="h5" sx={{ mr: 1, fontWeight: "medium" }}>
                Tamaño de letra
              </Typography>
              <Button
                onClick={incrementFontSize}
                sx={{ mr: 1 }}
                color={"primary"}
                variant="contained"
              >
                <Typography variant="button">
                  <AddIcon />
                </Typography>
              </Button>
              <Button
                onClick={decrementFontSize}
                color={"primary"}
                variant="contained"
              >
                <Typography variant="button">
                  <RemoveIcon />
                </Typography>
              </Button>
            </Box>
          </Box>
        </MenuModalButton>
      </Grid>
      <Grid item>
        <ModalProvider>
          <MenuModalButton
            classNameButton={`Help_Demo`}
            text="Ayuda"
            icon={<QuestionMarkIcon />}
            variant="contained"
          >
            <Box sx={{ m: 5 }}>
              <Box>
                <Typography variant="h3" sx={{ fontWeight: "bold", pb: 5 }}>
                  Ayuda
                </Typography>
              </Box>
              <Button
                onClick={() => {
                  localStorage.removeItem("alreadyVisited");
                  window.location.reload();
                  toggle();
                }}
                color={"primary"}
                variant="contained"
              >
                VOLVER A HACER EL TUTORIAL
              </Button>

              <Link to="/informacion">
                <Typography
                  variant="body1"
                  sx={{
                    pt: 3,
                    fontWeight: "medium",
                    color: theme.palette.secondary.contrastText,
                  }}
                >
                  Más información de interes
                </Typography>
              </Link>
            </Box>
          </MenuModalButton>
        </ModalProvider>
      </Grid>
      <Grid item>
        <Link to="/">
          <Button
            aria-label="volver"
            sx={{
              ...(!isMobile ? { m: 2 } : { m: 0.7 }),
              p: 2,
            }}
            variant="contained"
            color="secondary"
          >
            {<ArrowBackIcon />}
          </Button>
        </Link>
      </Grid>
    </Grid>
  );
}
