
import { Button, Box, Paper, Typography } from "@mui/material"
import Carousel from 'react-material-ui-carousel'
import { useState } from "react";
import { maxWidth } from "@mui/system";
import useMetersContext from "hooks/UseMetersContext";

interface ItemsInterface{
  name: string,
  image: string,
  year: number,
  description: string,
  meters: number
}
export default function CondicionesArchivadas() {
  const [description, setDescription] = useState("");
  const {setSliderMeters} = useMetersContext()

  var items:ItemsInterface[] = [
    {
      name: "INUNDACIÓN LA PLATA",
      image: "/inundación_2013.jpg",
      year: 2013,
      description: "El 2 de abril de 2013, la ciudad de La Plata y sus cercanías fueron afectadas por una lluvia continua. El Servicio Meteorológico Nacional informó que 181 mm cayeron entre las 18 y las 21, pudiendo suponerse un valor de hasta 200 mm en otras áreas no medidas; las ciudades de Ensenada y Berisso y los barrios platenses de Los Hornos, Villa Elvira y Tolosa fueron los más afectados junto al casco céntrico de la ciudad. La cantidad de víctimas fatales confirmadas fue de 89. Se registraron un total de 2200 evacuados",
      meters: 50
    },
    {
      name: "INUNDACIÓN CONCORDIA",
      image: "/inundación_1959.jpg",
      year: 1959,
      description: 'La inundación de abril de 1959, considerada en la Argentina un "desastre nacional", deja bajo las aguas una extensión que supera los 20.000 km2. Desde Concordia hasta el delta y durante tres meses, la provincia observa un continuo manto hídrico sobre tierras ricamente productivas',
      meters: 30
    },
    {
      name: "INUNDACIÓN SANTA FE",
      image: "/inundación_2007.jpg",
      year: 2007,
      description: "La inundación en Santa Fe de 2007 fue un suceso meteorológico ocurrido en el mes de marzo de ese año que afectó a la ciudad de Santa Fe y las localidades aledañas, a causa de fuertes precipitaciones que se sucedieron en la zona litoral argentina. En total, cayeron 437 milímetros de lluvia del 26 de marzo al 4 de abril, dejando como saldo 26 000 evacuados",
      meters: 60
    }
  ]

  function Item(props: any) {
    return (
      <Box>
        <Paper 
        onClick={() => setSliderMeters(props.item.meters) }
        sx={{
          backgroundImage: `url(${props.item.image})`,
          height: '9rem',
          backgroundRepeat: 'no-repeat',
          backgroundPosition: 'center',
          backgroundSize: 'cover',
          display: 'grid',
          padding: 3,
          cursor: 'pointer'
        }}>
          <Typography variant="h2" sx={{ fontWeight: "bold", textAlign: 'center', color: 'white', WebkitTextStroke: '1px black' }}>
            {props.item.name}
          </Typography>
          <Typography variant="h6" sx={{ fontWeight: "bold", textAlign: 'left', color: 'white', WebkitTextStroke: '1px black' }}>
            {props.item.year}
          </Typography>
        </Paper>
      </Box>

    )
  }

  return (
    <Box sx={{ m: 5 }}>
      <Box>
        <Typography variant="h3" sx={{ fontWeight: "bold", pb: 5 }}>
          Condiciones Archivadas
        </Typography>
        <Carousel
          indicators={false}
          animation="slide"
          navButtonsAlwaysVisible={true}
          cycleNavigation={false}
          autoPlay={false}
          navButtonsProps={{
            style: {
              backgroundColor: "white",
              color: "black"
            }
          }}
          changeOnFirstRender={true}
          onChange={(index) => {
            if (index != undefined) {
              setDescription(items[index].description)
            }
          }}
        >
          {
            items.map((item, i) => <Item key={i} item={item} />)
          }
        </Carousel>
        <Typography variant="subtitle2" sx={{ mt: 4, textAlign: 'left', maxWidth: '450px' }}>
          {/* {description} */}
        </Typography>
      </Box>
    </Box>
  )
}