import { VisibilityOff } from "@mui/icons-material";
import { Grid } from "@mui/material";
import { MetersProviderContext } from "context/MetersContext";
import BasicDemo from "components/Demo/Demo";
import Mapita from "components/Map/Mapita";
import Menu from "components/Menu/Menu";
import { getScreenSize } from "modules/helpers";
import { useState } from "react";
import { isMobile } from "react-device-detect";

export default function MapPage() {
  const [breakpoint, setBreakpoint] = useState(getScreenSize());
  return (
    <MetersProviderContext>
      <Grid container justifyContent="space-between" >
        <Grid item sx={{ zIndex: "1", ...(!isMobile ? {} : { position: 'absolute', bottom: 40, left: 5 }) }}>
          <Menu />
        </Grid>
        <Grid
          item
          sx={{
            position: "absolute",
            height: "100%",
            width: "100%",
          }}
        >
          <Mapita />
        </Grid>
        <Grid item sx={{ display: "none", }}>
          <BasicDemo breakpoint={breakpoint} />
        </Grid>
      </Grid>

    </MetersProviderContext>
  );
}
