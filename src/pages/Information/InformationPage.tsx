import { Box, Button, Divider, Typography, useTheme } from "@mui/material";
import Link from "@mui/material/Link";
import { Link as LinkRouter } from "react-router-dom";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

const LinksInteres = [
  {
    title:
      "Las inundaciones en La Plata, Berisso y Ensenada: análisis de riesgo, estrategias de intervención. Hacia la construcción de un observatorio ambiental.",
    description:
      "Los conflictos en torno a las inundaciones son de naturaleza multicausal, alertan sobre la necesidad de modificar la racionalidad de su manejo. Una de sus principales causas está relacionada con eventos de inundaciones por precipitaciones y/o sudestadas y sus consecuencias catastróficas a nivel regional, que han sido frecuentes en los últimos cien años y se agudizaron en las últimas décadas. La acción antrópica, que ignora el sistema hídrico, generalmente desemboca en una tragedia.",
    link: "http://sedici.unlp.edu.ar/handle/10915/59633",
  },
  {
    title:
      "Manual Plan de Reducción de Riesgo por Inundaciones en la Región de La Plata",
    description:
      "Este manual es un medio didáctico creado para aportar a la transformación y al diálogo de saberes relacionados a la ciudad y su riesgo de inundación. Incluye una propuesta pedagógica y estrategias para que quienes hagan uso de él puedan construir un diagnóstico del territorio que habitan, así como definir medidas particulares para la prevención y recomendaciones de acción ante la emergencia. ",
    link: "http://sedici.unlp.edu.ar/handle/10915/112896",
  },
  {
    title:
      "Informe Final PIO: Las inundaciones en La Plata, Berisso y Ensenada. Análisis de riesgos y estrategias de intervención.",
    description: "",
    link: "http://omlp.sedici.unlp.edu.ar/dataset/informe-final",
  },
];

export default function InformationPage() {
  const theme = useTheme();
  return (
    <Box sx={{ p: 3 , backgroundColor: theme.palette.background.paper, minHeight: "100vh"}}>
       <LinkRouter to="/map">
          <Button
            aria-label="Volver a mapa"
            variant="contained"
            sx={{
              p: 2,
            }}
            color="secondary"
          >
            {<ArrowBackIcon />}
          </Button>
        </LinkRouter>
        <Typography
          variant="h1"
          sx={{ fontWeight: "bold", color: theme.palette.primary.main }}
        >
          Información de interés
        </Typography>
        <ul>
          {LinksInteres.map((link) => {
            return (
              <Box sx={{ pb: 3}}>
                <li>
                  <Typography align="justify" variant="h4" sx={{ fontWeight: "bold", color: theme.palette.secondary.contrastText }}>
                    {link.title}
                  </Typography>
                  <Typography align="justify" variant="body2" sx={{ fontWeight: "ligth", color: theme.palette.secondary.contrastText }}>
                    {link.description}
                  </Typography>
                  <Typography variant="body2" sx={{ fontWeight: "ligth", color: theme.palette.secondary.contrastText }}>
                    <Link href={link.link} >
                      Leer más
                    </Link>
                  </Typography>
                  
                </li>
                <Divider sx={{pt:2}}/>
              </Box>
            );
          })}
        </ul>
      
    </Box>
  );
}
