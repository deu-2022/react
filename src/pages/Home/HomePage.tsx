import {
  Button,
  Typography,
  Grid,
  useTheme,
  IconButton,
  Divider,
  Paper,
  List,
  ListItem,
  ListItemText,
  ListItemIcon,
} from "@mui/material";
import Brightness1Icon from "@mui/icons-material/Brightness1";
import { Link as LinkRouter } from "react-router-dom";
import Link  from '@mui/material/Link';
import { Box } from "@mui/system";
import { HOME_CONFIG } from "config/Constant";
import Brightness4Icon from "@mui/icons-material/Brightness4";
import Brightness7Icon from "@mui/icons-material/Brightness7";
import { useContext } from "react";
import { ThemeContext } from "context/ThemeContext";

export default function HomePage() {
  const theme = useTheme();
  const { toggleColorMode } = useContext(ThemeContext);
  return (
    <Box
      sx={{
        backgroundColor: theme.palette.background.paper,
        minHeight: "100vh",
        weigth: "100vw",
      }}
    >
      <Grid
        container
        alignItems="end"
        justifyContent="end"
        spacing={1}
        sx={{ pb: 8 }}
      >
        <Grid item>
          <IconButton
            sx={{
              m: "0.8em 1em 0em 0em",
              color: theme.palette.secondary.contrastText,
            }}
            onClick={toggleColorMode}
            color="inherit"
            aria-label="Cambiar pagina a modo oscuro"
          >
            {theme.palette.mode === "dark" ? (
              <Brightness7Icon />
            ) : (
              <Brightness4Icon />
            )}
          </IconButton>
        </Grid>
      </Grid>
      <Grid
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
        sx={{ m: "0em 1em 0em 0em" }}
      >
        <Grid
          item
          md={5}
          xs={12}
          sx={{ m: "0em -1em 0em 1em", maxWidth: "90%" }}
        >
          <Box
            component="img"
            alt=""
            sx={{ height: "90%", width: "90%" }}
            src="/mapa.png"
          />
        </Grid>
        <Grid
          item
          md={5}
          xs={12}
          sx={{ m: "0em 1em 0em 1em ", color: theme.palette.primary.main }}
        >
          <Typography variant="h2" sx={{ fontWeight: "bold" }}>
            {HOME_CONFIG.title}
          </Typography>
          <Typography
            variant="subtitle1"
            sx={{ fontWeight: "medium", color: theme.palette.primary.main }}
          >
            {HOME_CONFIG.subtitle}
          </Typography>
          <Typography
            variant="body1"
            sx={{
              fontWeight: "medium",
              color: theme.palette.secondary.contrastText,
            }}
          >
            {HOME_CONFIG.description}
          </Typography>
          <LinkRouter to={"map"} style={{ textDecoration: "none" }}>
            <Button
              variant="contained"
              sx={{ borderRadius: "20px", mt: 3 }}
              color="primary"
            >
              <Typography
                variant="button"
                sx={{ fontWeight: "bold", m: "0.5em 0.8em 0.5em 0.8em" }}
              >
                Ver mapa
              </Typography>
            </Button>
          </LinkRouter>
        </Grid>
      </Grid>
      <Grid
        container
        direction="row"
        alignItems="center"
        justifyContent="center"
        sx={{ m: "0em 0em 9em 0em" }}
      >
        <Grid item md={5} xs={12} sx={{ mt: 16 }}>
          <Divider />
        </Grid>
      </Grid>
      <Grid
        container
        direction="row-reverse"
        alignItems="center"
        justifyContent="center"
        sx={{ pb: 2 }}
      >
        <Grid item md={5} xs={12} sx={{ pl: 15, pt: 1, m: "0em 1em 0em 1em" }}>
          <Box
            component="img"
            sx={{ height: "60%", width: "60%" }}
            alt=""
            src="/de_que_trata.png"
          />
        </Grid>
        <Grid item md={5} xs={12} sx={{ mb: 1 }}>
          <Paper sx={{ p: 5, m: "0em 1em 0em 1em" }}>
            <Typography variant="h3" sx={{ fontWeight: "bold", mb: 2 }}>
              ¿De que trata la app?
            </Typography>
            <Typography variant="body1" sx={{ fontWeight: "ligth", mb: 2 }}>
              Esta aplicación es una herramienta para simular una posible
              inundación en la ciudad de La Plata.
            </Typography>
            <Typography variant="h4" sx={{ fontWeight: "bold", mb: 2 }}>
              ¿Por qué simular?
            </Typography>
            <List dense={true}>
              <ListItem>
                <ListItemIcon>
                  <Brightness1Icon sx={{ fontSize: "small" }} />
                </ListItemIcon>
                <ListItemText
                  primaryTypographyProps={{
                    fontSize: 20,
                    lineHeight: "20px",
                    mb: "2px",
                  }}
                  primary="Poder tener una aproximación del estado de la ciudad frente a una inundación."
                />
              </ListItem>
              <ListItem>
                <ListItemIcon>
                  <Brightness1Icon sx={{ fontSize: "small" }} />
                </ListItemIcon>
                <ListItemText
                  primaryTypographyProps={{
                    fontSize: 20,
                    lineHeight: "20px",
                    mb: "2px",
                  }}
                  primary="Identificar que zonas serian las más afectadas en caso de una inundación."
                />
              </ListItem>
              <ListItem>
                <ListItemIcon>
                  <Brightness1Icon sx={{ fontSize: "small" }} />
                </ListItemIcon>
                <ListItemText
                  primaryTypographyProps={{
                    fontSize: 20,
                    lineHeight: "20px",
                    mb: "2px",
                  }}
                  primary="Ver el estado de una vivienda frente a una inundación."
                />
              </ListItem>
            </List>
            <Typography variant="h5" sx={{ fontWeight: "ligth" }}></Typography>
          </Paper>
        </Grid>

        <Grid
          container
          direction="row"
          alignItems="center"
          justifyContent="center"
          sx={{ pb: 5 }}
        >
          <Grid item md={5} xs={12} sx={{ ml: 10, mt: 6 }}>
            <Box
              component="img"
              sx={{ height: "70%", width: "70%" }}
              alt=""
              src="/deu.png"
            />
          </Grid>

          <Grid item md={6} xs={12} sx={{ mt: 8 }}>
            <Paper
              sx={{
                p: 5,
                backgroundColor: theme.palette.primary.main,
                color: theme.palette.secondary.main,
              }}
            >
              <Typography variant="h3" sx={{ fontWeight: "bold" }}>
                Sobre el proyecto
              </Typography>
              <Typography
                variant="body1"
                display="inline"
                sx={{ fontWeight: "ligth" }}
              >
                {
                  "Proyecto de la materia Diseño de Experiencia de Usuario y Diseño Centrado en el Usuario de la "
                }
              </Typography>
              <Typography
                variant="body1"
                display="inline"
                sx={{ fontWeight: "bold" }}
              >
                <Link href={"https://www.info.unlp.edu.ar/"} color="inherit">
                  Universidad Nacional de La Plata
                </Link>
              </Typography>
              <br />
              <Typography
                variant="body1"
                display="inline"
                sx={{ fontWeight: "ligth" }}
              >
                {"Con el objetivo de realizar una "}
              </Typography>
              <Typography
                variant="body1"
                display="inline"
                sx={{ fontWeight: "medium" }}
              >
                aplicacion que sea accessible para todos.
              </Typography>
            </Paper>
          </Grid>
        </Grid>
      </Grid>
    </Box>
  );
}
