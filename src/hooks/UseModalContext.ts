import { ModalContext } from "context/ModalContext";
import { useContext } from "react";

export default function useModalContext() {

    const context = useContext(ModalContext);
    if (context === undefined) {
      throw new Error("useModalContext must be used within a ModalContextProvider");
    }
    return context;
}