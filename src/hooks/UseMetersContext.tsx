import { MetersContext } from "context/MetersContext";
import { useContext } from "react";

export default function useMetersContext(){
  const context = useContext(MetersContext);
  if (context === undefined) {
    throw new Error("useMetersContext must be used within a MetersProvider");
  }
  return context;
}