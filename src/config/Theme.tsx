import { PaletteMode } from "@mui/material";
import {
  createTheme,
  ThemeOptions,
  experimental_sx as sx,
  ThemeProvider,
} from "@mui/material/styles";
import "fonts/Raleway/Raleway.ttf";
import { useEffect, useMemo, useState } from "react";
import { ThemeContext } from "context/ThemeContext";

export function ThemeProviderContext({
  children,
}: {
  children: React.ReactNode;
}) {
  const [themeMode, setThemeMode] = useState<PaletteMode>( localStorage["theme"] === "dark" ? "dark" : "light");
  const [fontSize, setFontSize] = useState<number>(localStorage["fontSize"] ? JSON.parse(localStorage["fontSize"]) : 1);
  const toggleColorMode = () =>{
    setThemeMode((prevMode) => (prevMode === "light" ? "dark" : "light"));
    }

  const incrementFontSize = () => setFontSize((prevSize) => prevSize < 1.4 ? prevSize + 0.2 : 1.4);
  const decrementFontSize = () => setFontSize((prevSize) => prevSize > 1 ? prevSize - 0.2 : 1);

  useEffect(() => {
    localStorage.setItem("fontSize", JSON.stringify(fontSize));
    localStorage.setItem("theme", themeMode)
  }, [fontSize,themeMode])
  
  const defaultTheme: ThemeOptions = useMemo(
    () =>
      createTheme({
        
        palette: {
          mode: themeMode,
          primary: {
            main: "#0288D1",
          },
          secondary: {
            main: themeMode == "light" ?"#fff": "#1e1e1e",
            dark: "#001E3C",
            contrastText:themeMode == "light" ? "#1e1e1e": "#fff",
          },
          text: {
            secondary: "#0288D1",
            disabled: "#b1b4bd",
          },
          error: {
            main: "#F2994A",
          },
          warning: {
            main: "#F44B4A",
          },
          success: {
            main: "#3ecd7b",
          },
        },

        typography: {
          fontFamily: "Raleway.ttf",
          fontWeightLight: 400,
          fontWeightRegular: 400,
          fontWeightMedium: 500,
          fontWeightBold: 800,
          h1: {
            fontSize: `${3 + fontSize}em`,
          },
          h2: {
            fontSize: `${2 + fontSize }em`,
          },
          h3: {
            fontSize: `${1.5 + fontSize}em`,
          },
          h4: {
            fontSize: `${0.7 + fontSize}em`,
          },
          h5: {
            fontSize: `${0.5 + fontSize}em`,
          },
          h6: {
            fontSize: `${0.4 + fontSize}em`,
          },
          subtitle1: {
            fontSize: `${0.2 + fontSize}em`,
          },
          body1: {
            fontSize: `${0.7 + fontSize}em`,
          },
          body2: {
            fontSize: `${0.4 + fontSize}em`,
          },
          button: {
            fontSize: `${0 + fontSize}em`,
            fontWeight: 800,
          },
        },
        components: {
          MuiPaper: {
            styleOverrides: {
              root: sx({
                borderRadius: "1em",
              }),
            },
          },
          MuiCard: {
            styleOverrides: {
              root: sx({
                borderRadius: "1em",
              }),
            },
          },
          MuiButton: {
            styleOverrides: {
              root: sx({
                "&:hover":{ 
                  backgroundColor: "#0288D1",
                },
              }),
            },
          },
        },
      }),
    [themeMode,fontSize]
  );

  return (
    <ThemeContext.Provider value={{ toggleColorMode, incrementFontSize, decrementFontSize }}>
      <ThemeProvider theme={defaultTheme}>
        {children}</ThemeProvider>
    </ThemeContext.Provider>
  );
}
