import { RouteObject } from "react-router-dom";
import { Dashboard, } from '@mui/icons-material';
import { ReactNode } from "react";
import HomeIcon from '@mui/icons-material/Home';
import ErrorIcon from '@mui/icons-material/Error';
import HomePage from "pages/Home/HomePage";


interface AppRouteObject extends RouteObject {
    name: string;
    icon: ReactNode;
}

export const appRoutes: AppRouteObject[] = [
    {
        name: "Inicio",
        path: "/",
        element: <HomePage />,
        icon: <HomeIcon />,
        children:[

        ]
    }

];

export default appRoutes;