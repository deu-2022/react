export const MAPBOX_TOKEN: string | undefined = process.env.REACT_APP_MAPBOX_TOKEN

export const MAP_STYLE_LIGHT: string = "mapbox://styles/juanpsm/cl4uqpovc003e14p5il6orxwf"
export const MAP_STYLE_DARK: string = "mapbox://styles/juanpsm/cl4w0ubor005a14pr6xsfzxph"

export const MAX_WATER_HEIGHT: number = 120;

interface HomeConfig {
    title: string,
    subtitle: string,
    description: string
}


export const HOME_CONFIG: HomeConfig = {
    title: "SIMULACION INUNDACIONES LA PLATA",
    subtitle: "UNLP - DEU y DCU 2022",
    description: "Esta aplicación es una herramienta para simular una posible inundación en la ciudad de La Plata"
}

