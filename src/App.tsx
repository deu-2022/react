
import { ThemeProviderContext } from "config/Theme";
import { BrowserRouter, Route, Routes } from "react-router-dom";
import HomePage from "pages/Home/HomePage";
import MapPage from "pages/Map/MapPage";
import InformationPage from "pages/Information/InformationPage";

function App() {
  return (
    <ThemeProviderContext>
      <BrowserRouter>
        <Routes>
          <Route path="/" element={<HomePage />} />
          <Route path="map" element={<MapPage />} />
          <Route path="informacion" element={<InformationPage />} />
        </Routes>
      </BrowserRouter>
    </ThemeProviderContext>
  );
}

export default App;
